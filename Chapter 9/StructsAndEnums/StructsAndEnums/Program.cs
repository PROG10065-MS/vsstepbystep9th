﻿#region Using directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#endregion

namespace StructsAndEnums
{
    class Program
    {
        static void doWork()
        {
            //create a default date
            Date defaultDate = new Date();
            Console.WriteLine($"The default date is {defaultDate}.");

            //create Valentine's day date
            Date valDate = new Date(14, Month.February, 2020);
            Console.WriteLine($"Valentine's Day is {valDate}.");
        }

        static void Main()
        {
            try
            {
                doWork();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
