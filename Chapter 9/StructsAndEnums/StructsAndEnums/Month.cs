﻿using System;

namespace StructsAndEnums
{
    //Define an enum for the months of the year
    public enum Month
    {
        January = 1,
        February,
        March,
        April,
        May,
        June,
        July,
        August,
        September,
        October,
        November,
        December
    }

}
