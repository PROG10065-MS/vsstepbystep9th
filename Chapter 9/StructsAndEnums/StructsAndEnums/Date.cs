﻿using System;

namespace StructsAndEnums
{
    // Define a structure that represents a Date
    public struct Date
    {
        private byte  _day;
        
        private Month _month;
        
        private short _year;

        public Date(byte day, Month month, short year)
        {
            _day = day;
            _month = month;
            _year = year;
        }

        //override ToString() so dates can be printed
        public override string ToString()
        {
            return $"{_month} {_day}, {_year}";
        }
    }
}
