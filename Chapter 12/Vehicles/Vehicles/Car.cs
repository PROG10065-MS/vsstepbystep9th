﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vehicles
{
	class Car : Vehicle // Car IS-A Vehicle
	{
		public Car()
		{
		}

		public Car(string make) : base(make)
		{
		}

		public override void Go()
		{
			this._mileage += 100;
			Console.WriteLine($"Vruummmmm goes the {_make} with {_speed} km/h");
		}
	}
}
