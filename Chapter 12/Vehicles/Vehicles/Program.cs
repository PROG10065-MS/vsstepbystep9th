﻿using System;

namespace Vehicles
{
    class Program
    {
        void DoWork()
        {
            //Generate a default generic vehicle
            Vehicle defaultVehicle = new Vehicle();

            //Create a generic vehicle
            Vehicle baseVehicle = new Vehicle("Basic Vehicle");
            baseVehicle.Speed = 100;
            baseVehicle.Speed += 20;
            baseVehicle.Go();

            //Create a default car
            Car defaultCar = new Car();
            defaultCar.Make = "Chevy";
            //Create a car object
            Car carObj = new Car("Honda");
            carObj.Go();

            //Create a plane object
            Airplane planeObj = new Airplane("AirBus A380");
            planeObj.Go();

            //Define a vehicle object variable to rever to the baseVehicle and ask it to go
            Vehicle vehicleObj = baseVehicle;
            vehicleObj.Go();

            //Define a vehicle object variable that refers to a car and ask it to go
            vehicleObj = carObj;
            vehicleObj.Go();

            //Use a vehicle object variable that refers to a plane and ask it to go
            vehicleObj = planeObj;
            vehicleObj.Go();
        }

        static void Main()
        {
            try
            {
                Program prog = new Program();
                prog.DoWork();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
