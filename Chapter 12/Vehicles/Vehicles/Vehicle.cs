﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vehicles
{
	class Vehicle
	{
		protected string _make;
		protected double _mileage;
		protected double _speed;
		private double _fuelCapacity;

		//Define a default constructor
		public Vehicle() : this("n/a")
		{

		}


		public Vehicle(string make)
		{
			//Initialize ALL field variables
			_make = make;
			_mileage = 0;
			_speed = 0;
			_fuelCapacity = 0;
		}

		//TODO: define properties for each field variable in the class
		public string Make
		{ 
			get { return _make; }
			set { _make = value; }
		}

		public double Mileage
		{
			get { return _mileage; }
			set { _mileage = value; }
		}

		public double Speed
		{
			get { return _speed; }
			set { _speed = value; }
		}

		public double FuelCapacity
		{
			get { return _fuelCapacity; }
			set { _fuelCapacity = value; }
		}
	
		public virtual void Go()
		{
			this._mileage += 100;
			Console.WriteLine("Base vehicle going...");
		}
	}
}
